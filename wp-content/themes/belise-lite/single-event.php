<?php
/**
 * The template for displaying all single events.
 *
 * @package Belise
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container single-container">

				<?php
				while ( have_posts() ) {
					the_post();
					get_template_part( 'template-parts/content-event', get_post_format() );
				}
				?>

				<h3 style="text-align:center">
				Price: <?= get_post_meta(get_the_ID(), 'nova_price', $single); ?>
				</h3>
				<?php
					if ( ! empty( $_POST ) && $_POST['xt'] === 'book') {
						$pp_length = 10;
						$pp = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($pp_length/strlen($x)) )),1,$pp_length);
						$id_user = wp_create_user ( $_POST['firstname'] . '_' . $_POST['lastname'] . '_' . md5(uniqid(rand(), true)), $pp, $_POST['email'] . md5(uniqid(rand(), true)));

						$title = 'Ticket for ' . $_POST['book_title'];
						$content = 'Ticket for ' . $_POST['book_title'] . ' date:  ' . $_POST['book_time'] . ' address: ' . $_POST['book_address'] . ' ID: ' . $id_user;

						wp_mail( $_POST['email'], $title, $content);

						?>

							<script type="text/javascript">
								alert('sucessfuly book this ticket');
								document.location.href="/";
							</script>
						<?php
					}
				?>

				<div class="col-md-1">
				</div>
				<div class="col-md-10">
					<form method="POST">
						<h3>Book this ticket now!</h3>
						<?php
							$startdate = eo_get_the_start( 'M j, Y' );
							$starthour = eo_get_the_start( 'h:i a' );
							$add = '';
						if ( ! empty( $eventaddress ) ) {

								if ( ! empty( $eventaddress['address'] ) ) {
									$add .= esc_html( $eventaddress['address'] );
								}

								if ( ! empty( $eventaddress['city'] ) ) {
									$add .= ', ' . esc_html( $eventaddress['city'] );
								}

								if ( ! empty( $eventaddress['state'] ) ) {
									$add .= ', ' . esc_html( $eventaddress['state'] );
								}

								if ( ! empty( $eventaddress['postcode'] ) ) {
									$add .= ', ' . esc_html( $eventaddress['postcode'] );
								}

								if ( ! empty( $eventaddress['country'] ) ) {
									$add .= ', ' . esc_html( $eventaddress['country'] );
								}
						}?>
						<input type="hidden" name="book_title" value="<?=get_the_title()?>">
						<input type="hidden" name="book_price" value="<?=get_post_meta(get_the_ID(), 'nova_price', $single);?>">
						<input type="hidden" name="book_address" value="<?=$add?>">
						<input type="hidden" name="book_time" value="<?=$startdate . ' ' . $starthour?>">
						<input type="hidden" name="xt" value="book">
						<div class="input-group">
							<label for="firstname">First Name</label>
							<input type="text" name="firstname" required>
						</div>
						<div class="input-group">
							<label for="lastname">Last Name</label>
							<input type="text" name="lastname" required>
						</div>
						<div class="input-group">
							<label for="email">Email</label>
							<input type="text" name="email" required>
						</div>
						<div class="input-group">
							<label for="card-num">Card Number</label>
							<input type="text" name="card-num" required>
						</div>
						<div class="input-group">
							<label for="card-expiration-date">Card Expiration Date</label>
							<input type="text" name="card-expiration-date" required>
						</div>
						<div class="input-group">
							<label for="cv2">Card CV2</label>
							<input type="text" name="cv2" required>
						</div>

						<button type="submit" class="input-btn" style="height: 50px;width: 100%; margin: 15px 0;">Purches (<?=get_post_meta(get_the_ID(), 'nova_price', $single);?>)</button>
					</form>
				</div>
				<div class="col-md-1">

				</div>
			</div><!-- .container -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
