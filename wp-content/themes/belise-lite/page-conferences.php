<?php /* Template Name: Conferences */ ?>
<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container single-container">
        <div class="col-md-12">
        <?php dynamic_sidebar('events_conferences'); ?>
      </div>
      </div>
		</main>
	</div>
<?php
get_footer();
