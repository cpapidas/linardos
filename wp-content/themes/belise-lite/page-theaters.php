<?php /* Template Name: Theaters */ ?>
<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container single-container">
        <div class="col-md-12">
        <?php dynamic_sidebar('events_theaters'); ?>
      </div>
      </div>
		</main>
	</div>
<?php
get_footer();
